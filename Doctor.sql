use example_DB

CREATE TABLE Secretery(
    SecreteryId INT PRIMARY KEY,
    Secretery VARCHAR(50)
)

CREATE TABLE Doctor(
    DoctorID INT PRIMARY KEY,
    Doctor VARCHAR(50),
    SecreteryId INT,
    CONSTRAINT fk_Secretary FOREIGN KEY (SecretaryID) REFERENCES Secretary(SecretaryID)
);

CREATE TABLE patient(
    PatientId INT PRIMARY KEY,
    Patient VARCHAR(50),
    DOB DATE,
    Address VARCHAR(50),
    DoctorId INT,
    Foreign Key (DoctorId) REFERENCES Doctor(DoctorId)
);

CREATE TABLE Prescription(
    PrescriptionId INT PRIMARY KEY,
    Drug VARCHAR(50),
    Date DATE,
    Dosage VARCHAR(50),
    PatientId INT,
    Foreign Key (PatientId) REFERENCES Patient(PatientId)
)
