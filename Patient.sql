USE example_DB;

CREATE TABLE PATIENT (
    PatientID INT PRIMARY KEY,
    Name VARCHAR(50),
    DOB DATE,
    Address VARCHAR(50)
);

CREATE TABLE SECRETORY (
    SecretaryID INT PRIMARY KEY,
    Name VARCHAR(255)
);

CREATE TABLE DOCTOR (
    DoctorID INT PRIMARY KEY,
    Doctor VARCHAR(50),
    SecretaryID INT,
    FOREIGN KEY (SecretaryID) REFERENCES Secretary(SecretaryID)
);


CREATE TABLE PRESCRIPTION (
    Prescription INT PRIMARY KEY,
    Drug VARCHAR(50),
    Date DATE, 
    Dosage VARCHAR(50),
    DoctorID INT,
    PatientID INT,
    FOREIGN KEY(DoctorID) REFERENCES Doctor(DoctorID),
    FOREIGN KEY(PatientID) REFERENCES Patient(PatientID)
);
