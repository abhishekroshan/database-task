USE example_DB

CREATE TABLE BRANCH(
    Branch VARCHAR(50) PRIMARY KEY,
    Branch_addr VARCHAR(50)
);

CREATE Table BOOK(
    ISBN VARCHAR(50) PRIMARY KEY,
    Title VARCHAR(50),
    Author VARCHAR(50),
    Publisher VARCHAR(50),
    Num_copies INT
);
