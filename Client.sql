use example_DB

CREATE TABLE ClIENT(
    Client INT PRIMARY KEY,
    Name VARCHAR(50),
    Location VARCHAR(50)
);

CREATE TABLE MANAGER(
    Manager INT PRIMARY KEY,
    Manager_name VARCHAR(50),
    Manager_location VARCHAR(50)
);

CREATE Table CONTRACT(
    Contract INT PRIMARY k,
    Estimated_cost INT,
    Completion_date INT
);

CREATE Table STAFF(
    Staff INT PRIMARY KEY,
    Staff_name VARCHAR(50),
    Staff_location VARCHAR(50)
);